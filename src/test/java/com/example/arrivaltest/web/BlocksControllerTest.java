package com.example.arrivaltest.web;

import com.example.arrivaltest.client.exception.BlockNotFoundException;
import com.example.arrivaltest.service.BlocksService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.CoreMatchers.is;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@AutoConfigureMockMvc
class BlocksControllerTest {
    @Autowired
    private MockMvc mvc;

    @MockBean
    private BlocksService blocksService;

    @Test
    void shouldValidateHeights() throws Exception {
        mvc.perform(get("/blocks/hash")).andExpect(status().isBadRequest());
        mvc.perform(get("/blocks/hash").param("blockStart", "1"))
                .andExpect(status().isBadRequest());
        mvc.perform(get("/blocks/hash").param("blockEnd", "1"))
                .andExpect(status().isBadRequest());
        mvc.perform(get("/blocks/hash")
                .param("blockStart", "0")
                .param("blockEnd", "101"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void shouldReturnSuccessResponse() throws Exception {
        when(blocksService.getCommonSubstring(1, 2)).thenReturn("abc");

        mvc.perform(get("/blocks/hash")
                .param("blockStart", "1").param("blockEnd", "2"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.substring", is("abc")));

    }

    @Test
    void shouldReturn404IfBlockNotFound() throws Exception {
        doThrow(new BlockNotFoundException("not found")).when(blocksService)
                .getCommonSubstring(100000, 100001);

        mvc.perform(get("/blocks/hash")
                .param("blockStart", "100000").param("blockEnd", "100001"))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message", is("not found")));
    }
}
