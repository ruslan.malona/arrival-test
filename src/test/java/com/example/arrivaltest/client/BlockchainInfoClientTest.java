package com.example.arrivaltest.client;

import com.example.arrivaltest.client.dto.Block;
import com.example.arrivaltest.client.dto.BlockResponse;
import com.example.arrivaltest.client.exception.BlockNotFoundException;
import com.example.arrivaltest.client.impl.BlockchainInfoClient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class BlockchainInfoClientTest {
    @Mock
    private RestTemplate restTemplate;
    @InjectMocks
    private BlockchainInfoClient blockClient;

    @Test
    void shouldReturnBlock() {
        var expectedBlock = mock(Block.class);
        when(restTemplate.getForEntity(anyString(), eq(BlockResponse.class))).thenReturn(ResponseEntity.ok(new BlockResponse(List.of(expectedBlock))));

        Block block = blockClient.getBlock(1);
        assertEquals(expectedBlock, block);
    }

    @Test
    void shouldThrowExceptionIfCannotGetBlock() {
        when(restTemplate.getForEntity(anyString(), eq(BlockResponse.class))).thenReturn(ResponseEntity.notFound().build());
        assertThrows(BlockNotFoundException.class, () -> blockClient.getBlock(1));
    }
}
