package com.example.arrivaltest.service;

import com.example.arrivaltest.service.impl.DefaultHashDecodeService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DefaultHashDecodeServiceTest {
    private final HashDecodeService hashDecodeService = new DefaultHashDecodeService();

    @Test
    void shouldDecodeSampleHash() {
        assertEquals("1471654128000", hashDecodeService.decodeHash("00000156A56BE980"));
        assertEquals("1056850286200300937094298967488490265669752758871228406",
                hashDecodeService.decodeHash("0000000000000000000b08b64f625bf4c51d0a34ebaef2b7451c0840149f7ff6"));
        assertEquals("1417115074586831187112255580375243348964882211567298911",
                hashDecodeService.decodeHash("0000000000000000000ecb9d7b39484bab8bde23859ce59a22ffef46735fe95f"));
    }

}
