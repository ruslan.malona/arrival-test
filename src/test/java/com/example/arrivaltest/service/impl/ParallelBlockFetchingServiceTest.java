package com.example.arrivaltest.service.impl;

import com.example.arrivaltest.client.dto.Block;
import com.example.arrivaltest.client.impl.BlockchainInfoClient;
import com.example.arrivaltest.client.exception.BlockNotFoundException;
import com.example.arrivaltest.service.BlockFetchingService;
import org.junit.jupiter.api.Test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

class ParallelBlockFetchingServiceTest {
    private final BlockchainInfoClient blockClient = mock(BlockchainInfoClient.class);
    private final ExecutorService executorService = Executors.newSingleThreadExecutor();
    private final BlockFetchingService testInstance = new ParallelBlockFetchingService(blockClient, executorService);

    @Test
    void shouldFetchNBlocks() {
        testInstance.fetchBlocks(1, 3);

        verify(blockClient).getBlock(1);
        verify(blockClient).getBlock(2);
        verify(blockClient).getBlock(3);
    }

    @Test
    void shouldThrowExceptionIfCannotGetSomeBlock() {
        when(blockClient.getBlock(1)).thenReturn(mock(Block.class));
        when(blockClient.getBlock(2)).thenThrow(new BlockNotFoundException(""));

        assertThrows(BlockNotFoundException.class, () -> testInstance.fetchBlocks(1, 2));
    }
}
