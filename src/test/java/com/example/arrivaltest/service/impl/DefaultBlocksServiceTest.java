package com.example.arrivaltest.service.impl;

import com.example.arrivaltest.client.dto.Block;
import com.example.arrivaltest.service.BlockFetchingService;
import com.example.arrivaltest.service.HashDecodeService;
import com.example.arrivaltest.service.LongestCommonSubstringService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DefaultBlocksServiceTest {
    @Mock
    private BlockFetchingService blockFetchingService;
    @Mock
    private HashDecodeService hashDecodeService;
    @Mock
    private LongestCommonSubstringService lcsService;
    @InjectMocks
    private DefaultBlocksService blocksService;

    @Test
    void shouldCheckAllPairs() {
        when(blockFetchingService.fetchBlocks(1, 3)).thenReturn(List.of(getBlock("a"), getBlock("b"), getBlock("c")));
        when(hashDecodeService.decodeHash(anyString())).thenReturn("1", "2", "3");
        when(lcsService.findSubstring(anyString(), anyString())).thenReturn("");

        blocksService.getCommonSubstring(1, 3);

        verify(lcsService).findSubstring("1", "2");
        verify(lcsService).findSubstring("1", "3");
        verify(lcsService).findSubstring("2", "3");
    }

    private Block getBlock(String hash) {
        Block block = new Block();
        block.setHash(hash);
        return block;
    }
}
