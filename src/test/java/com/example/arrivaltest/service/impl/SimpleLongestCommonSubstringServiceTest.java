package com.example.arrivaltest.service.impl;

import com.example.arrivaltest.service.LongestCommonSubstringService;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SimpleLongestCommonSubstringServiceTest {
    private final LongestCommonSubstringService service = new SimpleLongestCommonSubstringService();

    @Test
    void shouldFindAtTheBeginning() {
        String s1 = "abcaa";
        assertEquals("abc", service.findSubstring(s1, "abc"));
        assertEquals("abc", service.findSubstring(s1, "abcd"));
        assertEquals("abc", service.findSubstring(s1, "dabc"));
        assertEquals("abc", service.findSubstring(s1, "dabcd"));
    }

    @Test
    void shouldFindInTheMiddle() {
        String s1 = "babca";
        assertEquals("abc", service.findSubstring(s1, "abc"));
        assertEquals("abc", service.findSubstring(s1, "abcd"));
        assertEquals("abc", service.findSubstring(s1, "dabc"));
        assertEquals("abc", service.findSubstring(s1, "dabcd"));
    }

    @Test
    void shouldFindAtTheEnd() {
        String s1 = "babc";
        assertEquals("abc", service.findSubstring(s1, "abc"));
        assertEquals("abc", service.findSubstring(s1, "abcd"));
        assertEquals("abc", service.findSubstring(s1, "dabc"));
        assertEquals("abc", service.findSubstring(s1, "dabcd"));
    }

    @Test
    void shouldNotFindIfAbsent() {
        assertEquals("", service.findSubstring("def", "abc"));
    }

    @Test
    void shouldNotReturnOneLetterMatch() {
        String s1 = "abc";
        assertEquals("", service.findSubstring(s1, "ade"));
        assertEquals("", service.findSubstring(s1, "dae"));
        assertEquals("", service.findSubstring(s1, "dea"));
    }
}
