package com.example.arrivaltest.service.impl;

import com.example.arrivaltest.client.dto.Block;
import com.example.arrivaltest.client.impl.BlockchainInfoClient;
import com.example.arrivaltest.service.BlockFetchingService;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executor;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
@RequiredArgsConstructor
public class ParallelBlockFetchingService implements BlockFetchingService {

    private final BlockchainInfoClient blockClient;
    private final Executor executor;

    @Override
    @SneakyThrows
    public List<Block> fetchBlocks(Integer startHeight, Integer endHeight) {

        var futureList = IntStream.rangeClosed(startHeight, endHeight)
                .mapToObj(this::blockSupplier)
                .map(r -> CompletableFuture.supplyAsync(r, executor))
                .collect(Collectors.toList());
        try {
            return CompletableFuture.allOf(futureList.toArray(new CompletableFuture[0]))
                    .thenApply(aVoid -> futureList.stream()
                            .map(CompletableFuture::join)
                            .collect(Collectors.toList()))
                    .get();
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            return List.of();
        } catch (ExecutionException e) {
            throw e.getCause();
        }
    }

    private Supplier<Block> blockSupplier(Integer blockHeight) {
        return () -> blockClient.getBlock(blockHeight);
    }
}
