package com.example.arrivaltest.service.impl;

import com.example.arrivaltest.service.LongestCommonSubstringService;
import org.springframework.stereotype.Service;

@Service
/*
 * Runtime complexity is N^2 if both strings have same size,
 * but since strings are not super-long (68 chars) and we have limited number of them (100 max)
 * this implementation is Ok for POC. It takes max 100ms to process 100 strings which is far less
 * than time spent for fetching block hashes via API.
 * Furthermore, we can easily replace this algorithm with the one which has linear time in future if we need to
 */
public class SimpleLongestCommonSubstringService implements LongestCommonSubstringService {
    @Override
    public String findSubstring(String s1, String s2) {
        String longest = "";
        for (int i = 0; i < s2.length() - 1; i++) {
            String subString = "";
            for (int j = i + 1; j <= s1.length(); j++) {
                if (s2.contains(s1.substring(i, j))) {
                    subString = s1.substring(i, j);
                } else {
                    break;
                }
            }
            if (subString.length() > 1 && subString.length() > longest.length()) {
                longest = subString;
            }
        }
        return longest;
    }
}
