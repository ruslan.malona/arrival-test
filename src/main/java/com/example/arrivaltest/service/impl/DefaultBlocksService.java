package com.example.arrivaltest.service.impl;

import com.example.arrivaltest.service.BlockFetchingService;
import com.example.arrivaltest.service.BlocksService;
import com.example.arrivaltest.service.HashDecodeService;
import com.example.arrivaltest.service.LongestCommonSubstringService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
@Slf4j
public class DefaultBlocksService implements BlocksService {

    private final BlockFetchingService blockFetchingService;
    private final HashDecodeService hashDecodeService;
    private final LongestCommonSubstringService lcsService;

    @Override
    public String getCommonSubstring(Integer blockStart, Integer blockEnd) {
        log.debug("Fetching blocks");
        var blocks = blockFetchingService.fetchBlocks(blockStart, blockEnd);

        var hashes = blocks.stream().map(b -> hashDecodeService.decodeHash(b.getHash())).collect(Collectors.toList());

        if (log.isDebugEnabled()) {
            log.debug("Hashes\n{}", String.join("\n", hashes));
        }

        var longestSubstring = "";
        for (int i = 0; i < hashes.size() - 1; i++) {
            for (int j = i + 1; j < hashes.size(); j++) {
                var substring = lcsService.findSubstring(hashes.get(i), hashes.get(j));
                if (substring.length() > longestSubstring.length()) {
                    longestSubstring = substring;
                }
            }
        }

        log.debug("Longest substring: {}", longestSubstring);

        return longestSubstring;
    }
}
