package com.example.arrivaltest.service.impl;

import com.example.arrivaltest.service.HashDecodeService;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
public class DefaultHashDecodeService implements HashDecodeService {
    @Override
    public String decodeHash(String hash) {
        BigInteger result = new BigInteger(hash, 16);
        return result.toString();
    }
}
