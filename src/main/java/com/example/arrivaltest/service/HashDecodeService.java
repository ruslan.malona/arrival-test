package com.example.arrivaltest.service;

public interface HashDecodeService {
    String decodeHash(String hash);
}
