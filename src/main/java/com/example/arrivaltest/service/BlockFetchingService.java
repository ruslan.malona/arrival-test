package com.example.arrivaltest.service;

import com.example.arrivaltest.client.dto.Block;

import java.util.List;

public interface BlockFetchingService {
    List<Block> fetchBlocks(Integer startHeight, Integer endHeight);
}
