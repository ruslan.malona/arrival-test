package com.example.arrivaltest.service;

public interface LongestCommonSubstringService {
    String findSubstring(String s1, String s2);
}
