package com.example.arrivaltest.service;

public interface BlocksService {
    String getCommonSubstring(Integer blockStart, Integer blockEnd);
}
