package com.example.arrivaltest.config;

import com.example.arrivaltest.client.impl.BlockConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.Executor;

@Configuration
public class ApplicationConfig {
    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }

    @Bean
    public Executor blockFetchingExecutor(@Value("${threadpool.threadCount:1}") Integer threadsCount,
                                          @Value("${threadpool.awaitTerminationSec:60}") Integer awaitTerminationSec) {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(threadsCount);
        executor.setAwaitTerminationSeconds(awaitTerminationSec);
        executor.setThreadNamePrefix("blockFetching-");
        return executor;
    }

    @Bean
    public HttpMessageConverters blockNotFoundConverter() {
        BlockConverter additional = new BlockConverter();
        return new HttpMessageConverters(additional);
    }
}
