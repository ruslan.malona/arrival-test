package com.example.arrivaltest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

@SpringBootApplication
@EnableRetry
public class ArrivalTestApplication {
    public static void main(String[] args) {
        SpringApplication.run(ArrivalTestApplication.class, args);
    }
}
