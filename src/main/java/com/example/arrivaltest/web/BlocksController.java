package com.example.arrivaltest.web;

import com.example.arrivaltest.service.BlocksService;
import com.example.arrivaltest.web.dto.BlocksRequest;
import com.example.arrivaltest.web.dto.BlocksResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/blocks")
@RequiredArgsConstructor
public class BlocksController {
    private final BlocksService blocksService;

    @GetMapping("/hash")
    public ResponseEntity<BlocksResponse> processBlocks(@Valid BlocksRequest request) {
        var commonSubstring = blocksService.getCommonSubstring(request.getBlockStart(), request.getBlockEnd());
        return ResponseEntity.ok(new BlocksResponse(commonSubstring));
    }
}
