package com.example.arrivaltest.web.dto;

import com.example.arrivaltest.web.RequestConstraint;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequestConstraint
public class BlocksRequest {
    private Integer blockStart;
    private Integer blockEnd;
}
