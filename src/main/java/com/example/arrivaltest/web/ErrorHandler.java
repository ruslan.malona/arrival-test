package com.example.arrivaltest.web;

import com.example.arrivaltest.client.exception.BlockFetchException;
import com.example.arrivaltest.client.exception.BlockNotFoundException;
import com.example.arrivaltest.web.dto.ApiError;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@Slf4j
public class ErrorHandler {
    @ExceptionHandler(BindException.class)
    public ResponseEntity<Object> handleValidationException(BindException e) {
        log.warn("Validation exception: {}", e.getMessage());
        return ResponseEntity.status(400).body(new ApiError("Invalid arguments"));
    }

    @ExceptionHandler(BlockFetchException.class)
    public ResponseEntity<Object> handleBlockFetchingException(BlockFetchException e) {
        log.error("Processing exception", e);
        return ResponseEntity.status(500).body(new ApiError(e.getMessage()));
    }
    @ExceptionHandler(BlockNotFoundException.class)
    public ResponseEntity<Object> handleBlockNotFound(BlockNotFoundException e) {
        log.error("Block not found: {}", e.getMessage());
        return ResponseEntity.status(404).body(new ApiError(e.getMessage()));
    }
}
