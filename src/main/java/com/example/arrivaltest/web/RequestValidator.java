package com.example.arrivaltest.web;

import com.example.arrivaltest.web.dto.BlocksRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RequestValidator implements ConstraintValidator<RequestConstraint, BlocksRequest> {
    @Override
    public boolean isValid(BlocksRequest value, ConstraintValidatorContext context) {
        var start = value.getBlockStart();
        var end = value.getBlockEnd();
        if (start == null || start < 0) {
            return false;
        } else if (end == null || end < 1) {
            return false;
        } else if (end - start > 100) {
           return false;
        } else {
            return end > start;
        }
    }
}
