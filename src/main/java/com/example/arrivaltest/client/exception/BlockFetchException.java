package com.example.arrivaltest.client.exception;

public class BlockFetchException extends RuntimeException {
    public BlockFetchException(String message, Throwable cause) {
        super(message, cause);
    }
}
