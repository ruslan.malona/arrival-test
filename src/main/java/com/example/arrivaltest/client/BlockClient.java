package com.example.arrivaltest.client;

import com.example.arrivaltest.client.dto.Block;

public interface BlockClient {
    Block getBlock(Integer blockHeight);
}
