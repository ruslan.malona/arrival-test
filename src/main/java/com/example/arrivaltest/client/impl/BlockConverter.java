package com.example.arrivaltest.client.impl;

import com.example.arrivaltest.client.dto.BlockResponse;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.http.converter.AbstractHttpMessageConverter;
import org.springframework.http.converter.HttpMessageNotReadableException;

import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class BlockConverter extends AbstractHttpMessageConverter<BlockResponse> {

    public BlockConverter() {
        super(new MediaType("text", "plain"));
    }

    @Override
    protected boolean supports(Class<?> clazz) {
        return BlockResponse.class.isAssignableFrom(clazz);
    }

    @Override
    protected BlockResponse readInternal(Class<? extends BlockResponse> clazz, HttpInputMessage inputMessage) throws IOException {
        String requestBody = toString(inputMessage.getBody());
        if ("Block not found".equals(requestBody)) {
            throw new HttpMessageNotReadableException("Cannot read", inputMessage);
        }
        return new BlockResponse();
    }

    @Override
    protected void writeInternal(BlockResponse block, HttpOutputMessage outputMessage) {
        // not used
    }

    private static String toString(InputStream inputStream) {
        Scanner scanner = new Scanner(inputStream, StandardCharsets.UTF_8);
        return scanner.useDelimiter("\\A").next();
    }
}
