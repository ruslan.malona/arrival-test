package com.example.arrivaltest.client.impl;

import com.example.arrivaltest.client.BlockClient;
import com.example.arrivaltest.client.dto.Block;
import com.example.arrivaltest.client.dto.BlockResponse;
import com.example.arrivaltest.client.exception.BlockNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.retry.annotation.Retryable;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

@Component
@RequiredArgsConstructor
@Slf4j
public class BlockchainInfoClient implements BlockClient {
    public static final String BLOCK_ENDPOINT = "https://blockchain.info/block-height/%d?format=json";
    private final RestTemplate restTemplate;

    @Retryable(include = {ResourceAccessException.class})
    public Block getBlock(Integer blockHeight) {
        var url = String.format(BLOCK_ENDPOINT, blockHeight);
        var response = restTemplate.getForEntity(url, BlockResponse.class);
        if (response.getStatusCode() == HttpStatus.OK && response.getBody() != null &&
                !CollectionUtils.isEmpty(response.getBody().getBlocks())) {
            log.debug("Fetched block with height={}", blockHeight);
            return response.getBody().getBlocks().get(0);
        } else {
            throw new BlockNotFoundException("Block " + blockHeight + " not found");
        }
    }
}
