# Bitcoin block hashes analyzer application

## Requirements

Implement RESTful web service that will analyze bitcoin blockchain block hashes. 
In addition to the common flow the service should have a propper error handling.
The service should expose an endpoint which will do the following:

- Accepts a block heights range. Maximum blocks count per request is 100
- Queries the data from the Bitcoin blockchain API (https://btc.com/api-doc#Block is a good way to go with) for the given block height range
- Calculates the longest sub-string hash found in more than one block hashes. Just to clarify, that block hash is a 256-bit number stored as a big-endian hexadecimal string. And by the "sub-string" we should understand a contiguous sequence of numbers

For example, for block heigths range 646940-646941 there are two blocks with hashes `0000000000000000000b08b64f625bf4c51d0a34ebaef2b7451c0840149f7ff6` and `0000000000000000000ecb9d7b39484bab8bde23859ce59a22ffef46735fe95f` respectivelly.
The decoded corresponding hash numbers are: `1056850286200300937094298967488490265669752758871228406` and `1417115074586831187112255580375243348964882211567298911` respectivelly.
The longest contiguous sequence of numbers for these hashes would be 2989 which should be returned by the service.

Note:
The bitcoin blockchain is a public decentralized network launched in 2009 which generates a new block every 10 minutes (approximately) starting from the "genesis" block i.e. a block with height = 0.
Block height is an integer number indicating a count of blocks starting from the "genesis" block.`

## Rest API

### Find the longest sub-string of hashes
To run the computation send **GET** request to` /blocks/hash` endpoint. 
It should have to mandatory parameters:
- `blockStart` height of the first block
- `blockEnd` height of the last block

Parameter constraints:
- `blockStart` and `blockEnd` must be >= 0, `blockStart` < `blockEnd`, `blockEnd` - `blockStart` <= 100

Responses:
- **200** if everything OK, with body:
```
{
  "substring": "98316"
}
```
- **400** if parameter validation error:
```
{
  "message": "Invalid arguments"
}
```
- **404** if such block height does not exist

## Installation and execution details
### Prerequisites
Technical requirements: JDK 11
### Start server
To start server execute following command in directory with project:
`./gradlew bootRun`
### Start using:
If no errors appeared in console, server is available at `http://localhost:8080`. If this port is busy, you can change it in `src/main/resources/application.properties`

## Implementation Notes
For searching commons substring of two strings I'm using the most straightforward approach which has O(n<sup>2</sup>) complexity

It is implemented in `SimpleLongestCommonSubstringService` and in future can be replaced with more performant algorithm

But in this initial implementation I assumed that it's a working solution because:
* strings (hashes) have small length (68 chars)
* there could be up to 100 of them overall
* network latency takes several times of magnitude more time than running this algorithm
